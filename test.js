const jsonLogic = require('./index.js')
const assert = require('assert')

it('calculate months difference', () => {
	assert.strictEqual(jsonLogic.apply({
		"if": [
			{
				"==": [
					{
						"var": "renewalTradeLicense"
					},
					true
				]
			},
			{
				"*": [
					0.15,
					{
						"+": [
							1,
							{
								"monthsDiff": [
									// We pass future, what we believe is future first!!!
									{
										"var": "newLicenseIssueDate"
									},
									{
										"var": "dateWhenLastLicenseExpired"
									}
								]
							}
						]
					}
				]
			}]
	}, {
		"renewalTradeLicense": true,
		"newLicenseIssueDate": "2020-01-01T00:00:00.000Z",
		"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
	}), 1.95)
});

it('check daysDiff', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"daysDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-02-11T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 41)
});

it('check weeksDiff', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"weeksDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-02-12T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 6)
});

it('check weeksDiff 2', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"weeksDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-01-07T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 0)
});

it('check weeksDiff 3', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"weeksDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-01-08T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 1)
});

it('check daysDiff one input null', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"daysDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-02-11T00:00:00.000Z",
			"dateWhenLastLicense": "2019-01-01T00:00:00.000Z"
		}), 0)
});

it('check monthsDiff', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"monthsDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-02-11T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 1)
});

it('check monthsDiff (floor)', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"monthsDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				},
				{
					"var": "func"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-02-11T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z",
			"func": 0
		}), 1)
});

it('check monthsDiff (ceil)', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"monthsDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				},
				{
					"var": "func"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-02-11T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z",
			"func": 1
		}), 2)
});
it('check yearsDiff', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"yearsDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-02-11T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 0)
});

it('check yearsDiff', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"yearsDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-02-11T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2014-01-01T00:00:00.000Z"
		}), 5)
});

it('yearFromDate', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"yearFromDate": [
				{
					"var": "newLicenseIssueDate"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-01-01T00:00:00.000Z",
		}), 2019)
});

it('monthFromDate', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"monthFromDate": [
				{
					"var": "newLicenseIssueDate"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-12-01T00:00:00.000Z",
		}), 12)
});

it('dateFromDate', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"dayFromDate": [
				{
					"var": "newLicenseIssueDate"
				}
			]
		},
		{
			"newLicenseIssueDate": "2019-01-23T00:00:00.000Z",
		}), 23)
});


it('check monthsDiff over year', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"monthsDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2020-01-01T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 12)
});

it('check monthsDiff over year plus one day', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"monthsDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2020-01-02T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 12)
});

it('check month is zero', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"monthsDiff": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "dateWhenLastLicenseExpired"
				}
			]
		},
		{
			"newLicenseIssueDate": "2018-01-01T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-01T00:00:00.000Z"
		}), 0)
});

it('parse dates, date empty', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"date_parse": {
				"var": "dateWhenLastLicenseExpired"
			}
		}, {"dateWhenLastLicenseExpired": ""}
	), null)
});

it('get date, date yyyy-MM-ddThh:mm:ss:xxxZ', () => {
	assert.strictEqual(jsonLogic.apply(
		{"getDate": {"var": "data.applicantBirthdate"}},
		{"data": { "applicantBirthdate": "2000-01-01T00:00:00.000Z"}}
	), "2000-01-01T00:00:00.000Z")
});

it('get date, epoch date', () => {
	assert.strictEqual(jsonLogic.apply(
		{"getDate": {"var": "data.applicantBirthdate"}},
		{"data": { "applicantBirthdate": 946684800000}}
	), "2000-01-01T00:00:00.000Z")
});

it('get date, date empty', () => {
	assert.strictEqual(jsonLogic.apply(
		{"getDate": {"var": "data.applicantBirthdate"}},
		{"data": {}}
	), null)
});

it('compare dates', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			">=": [{
				"date_parse": {
					"var": "dateWhenLastLicenseExpired"
				}
			}, {
				"date_parse": {
					"var": "newLicenseIssueDate"
				}
			}]
		},
		{
			"newLicenseIssueDate": "2018-01-01T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-02T00:00:00.000Z"
		}), true)
});

it('compare dates', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			">=": [{
				"date_parse": {
					"var": "dateWhenLastLicenseExpired"
				}
			}, {
				"date_parse": {
					"var": "newLicenseIssueDate"
				}
			}]
		},
		{
			"newLicenseIssueDate": "2020-01-01T00:00:00.000Z",
			"dateWhenLastLicenseExpired": "2019-01-02T00:00:00.000Z"
		}), false)
});

it('compare dates when one of them is actually a date', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			">=": [{
				"date_parse": {
					"var": "dateWhenLastLicenseExpired"
				}
			}, {
				"date_parse": {
					"var": "newLicenseIssueDate"
				}
			}]
		},
		{"newLicenseIssueDate": new Date(), "dateWhenLastLicenseExpired": "2019-01-02T00:00:00.000Z"}), false)
});

it('substract days', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"substractDays": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "numOfDays"
				},
			]
		},
		{"newLicenseIssueDate": "2020-01-20T00:00:00.000Z", "numOfDays": 19}), '2020-01-01T00:00:00.000Z')
});

it('substract days over year', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"substractDays": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "numOfDays"
				},
			]
		},
		{"newLicenseIssueDate": "2020-01-01T00:00:00.000Z", "numOfDays": 365}), '2019-01-01T00:00:00.000Z')
});
it('add days', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"addDays": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "numOfDays"
				},
			]
		},
		{"newLicenseIssueDate": "2020-01-20T00:00:00.000Z", "numOfDays": 19}),'2020-02-08T00:00:00.000Z')
});

it('add days over year', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"addDays": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "numOfDays"
				},
			]
		},
		{"newLicenseIssueDate": "2020-01-01T00:00:00.000Z", "numOfDays": 365}), '2020-12-31T00:00:00.000Z')
});
it('substract months over year', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"substractMonths": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "numOfMonths"
				},
			]
		},
		{"newLicenseIssueDate": "2020-01-01T00:00:00.000Z", "numOfMonths": 13}), '2018-12-01T00:00:00.000Z')
});
it('add months', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"addMonths": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "numOfMonths"
				},
			]
		},
		{"newLicenseIssueDate": "2020-01-20T00:00:00.000Z", "numOfMonths": 3}), '2020-04-20T00:00:00.000Z')
});

it('substract years', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"substractYears": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "numOfYears"
				},
			]
		},
		{"newLicenseIssueDate": "2019-01-20T00:00:00.000Z", "numOfYears": 19}), '2000-01-20T00:00:00.000Z')
});

it('formatDateDDdashMMdashYYYY( add years integer)', () => {
	assert.strictEqual(jsonLogic.apply(
		{"formatDateDDdashMMdashYYYY": [{
			"addYears": [
				{
					"var": "newLicenseIssueDate"
				},
				{ "var": "numOfYears" },
			]
		}]},
		{"newLicenseIssueDate": "2020-03-31", "numOfYears": 2}), '31-03-2022')
});

it('formatDateDDdashMMdashYYYY( add years integer second) ', () => {
	assert.strictEqual(jsonLogic.apply(
		{ "formatDateDDdashMMdashYYYY": [{
			"addYears": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"-": [2020, { "var": "numOfYears" }],
				},
			]
		}]},
		{"newLicenseIssueDate": "1900-04-01", "numOfYears": 1900}), '01-04-2020')
});

it('add years float', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"addYears": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "numOfYears"
				},
			]
		},
		{"newLicenseIssueDate": "2019-01-20T00:00:00.000Z", "numOfYears": 0.12}), '2019-02-20T00:00:00.000Z')
});

it('override year first', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"overrideYear": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "year"
				},
			]
		},
		{"newLicenseIssueDate": "2020-04-01T00:00:00.000Z", "year": 2000 }), '2000-04-01T00:00:00.000Z')
});

it('override year second', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"overrideYear": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "year"
				},
			]
		},
		{"newLicenseIssueDate": new Date('2020-04-01'), "year": 2000 }), '2000-04-01T00:00:00.000Z')
});

it('override year null', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"overrideYear": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "year"
				},
			]
		},
		{"newLicenseIssueDate": new Date('2020-04-01'), "yeaer": 2000 }), '2020-04-01T00:00:00.000Z')
});

it('override year both nulls', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"overrideYear": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "year"
				},
			]
		},
		{"newLicenseIssueDatee": new Date('2020-04-01'), "yeaer": 2000 }), null)
});

it('override date all sections', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"overrideDate": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "year"
				},
				5,
				20,
				15,
				55
			]
		},
		{"newLicenseIssueDate": new Date('2020-04-01'), "year": 2000 }), '2000-05-20T15:55:00.000Z')
});

it('override date month and day sections', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"overrideDate": [
				{
					"var": "newLicenseIssueDate"
				},
				{
					"var": "year"
				},
				12,
				31
			]
		},
		{"newLicenseIssueDate": new Date('2020-04-01'), "year": undefined }), '2020-12-31T00:00:00.000Z')
});

it('override date month section', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"overrideDate": [
				{
					"var": "newLicenseIssueDate"
				},
				0,
				5
			]
		},
		{"newLicenseIssueDate": new Date('2020-04-01'), "yeaer": 2000 }), '2020-05-01T00:00:00.000Z')
});

it('override date first argument only', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"overrideDate": [
				{
					"var": "newLicenseIssueDate"
				}
			]
		},
		{"newLicenseIssueDate": new Date('2020-04-01'), "yeaer": 2000 }), '2020-04-01T00:00:00.000Z')
});

it('format date', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"formatDate": [
				{
					"var": "newLicenseIssueDate"
				},
				"YYYY/MM/DD",
				"YYYY-MM-DD",
			]
		},
		{"newLicenseIssueDate": "1984/08/05"}), "1984-08-05")
});

it('first applicable date ', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"firstApplicableDate": [
				{"if": [true]},
				{"if": [false]},
				3,
				6,
				null,
				undefined,
				{
					"var": "data.applicantBirthadate"
				},
				{"addYears":[{"var":"data.applicantBirthdate"},10]}, // this is victorious
				{"addYears":["2001-01-20T00:00:00.000Z",10]},
				new Date("2029-01-20T00:00:00.000Z"),
			]
		},
		{"data": { "applicantBirthdate": "2019-01-20T00:00:00.000Z"}}), "2029-01-20T00:00:00.000Z")
});

it('first applicable date isostring date', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"firstApplicableDate": [
				{"if": [true]},
				{"if": [false]},
				3,
				6,
				null,
				undefined,
				{
					"var": "data.applicantBirthdate"
				},
			]
		},
		{"data": { "applicantBirthdate": "2019-01-20T00:00:00.000Z"}}), "2019-01-20T00:00:00.000Z")
});

it('first applicable date no date', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"firstApplicableDate": [
				{"if": [true]},
				{"if": [false]},
				3,
				6,
				null,
				undefined,
				{
					"var": "data.applicantBirthdate"
				},
			]
		},
		{"data": { "applicantBirthdate": "2019-21-20T00:00:00.000Z"}}), null)
});

it('first applicable date empty list', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"firstApplicableDate": []
		},
		{"data": { "applicantBirthdate": "2019-01-20T00:00:00.000Z"}}), null)
});

it('round 1.5', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"round": [{"var": "data.onePointFive"}]
		},
		{"data": { "onePointFive": 1.5}}), 2)
});

it('round 1.5530', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"round": [{"var": "data.onePointFiveFiveThreeZero"}, {"var": "data.two"}]
		},
		{"data": { "onePointFiveFiveThreeZero": 1.5530, "two": 2}}), 1.55)
});

it('round fail', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"round": [{"var": "data.sixty"}, {"var": "data.two"}]
		},
		{"data": { "onePointFiveFiveThreeZero": 1.5530, "two": 2}}), undefined)
});

it('ceil 1.2', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"ceil": [{"var": "data.onePointTwo"}]
		},
		{"data": { "onePointTwo": 1.2}}), 2)
});

it('floor 1.9', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"floor": [{"var": "data.onePointNine"}]
		},
		{"data": { "onePointNine": 1.9}}), 1)
});

it('parse int', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"parseInt": [{"var": "data.onePointNine"}]
		},
		{"data": { "onePointNine": "1.9"}}), 1)
});

it('parse float', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"parseFloat": [{"var": "data.onePointNine"}]
		},
		{"data": { "onePointNine": "1.9"}}), 1.9)
});

it('join', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"join": [[{"var": "data.firstName"}, {"var": "data.lastName"}], " "]
		},
		{"data": { "firstName": "John", "lastName": "Smith"}}), "John Smith")
});

it('split', () => {
	assert.strictEqual(JSON.stringify(jsonLogic.apply(
		{
			"split": [{"var": "data.name"}, " "]
		},
		{"data": { "name": "John Smith"}})), "[\"John\",\"Smith\"]")
});

it('replace', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"replace": [{"var": "data.nit"}, "-", ""]
		},
		{"data": { "nit": "0614-050788-110-4"}}), "06140507881104")
});

it('replace needed for lesotho', () => {
	assert.strictEqual(jsonLogic.apply(
		{"replace": [{"getDate": {"var": "data.guidePreviousExpiryDate"}}, /(?<=\d\d\d\d-\d\d-\d\d).*/, ""]},
		{"data": {"guidePreviousExpiryDate": "2020-01-02T00:00:00.000Z"}}), "2020-01-02")
});


it('toUpperCase', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"toUpperCase": {
				"join": [[{"var": "data.firstName"}, {"var": "data.lastName"}], " "]
			}
		},
		{"data": { "firstName": "John", "lastName": "Smith"}}), "JOHN SMITH")
});

it('toLowerCase', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"toLowerCase": {
				"join": [[{"var": "data.firstName"}, {"var": "data.lastName"}], " "]
			}
		},
		{"data": { "firstName": "John", "lastName": "Smith"}}), "john smith")
});

it('dataGridColumnTotal', () => {
	assert.strictEqual(jsonLogic.apply(
		{"dataGridColumnTotal": "data.applicantDataGrid_collection_applicantBaseSalary"},
		{"data":{"applicantDataGrid": [{"applicantFirstName3":"Ali","employeeId":"EMPLOYEE1","employeesFullName":"Ali  ","confirm":false,"applicantEmployerContribution":2000,"applicantEmployeeDeduction":480,"applicantHealth":40,"applicantInjury":80,"applicantPension":560,"applicantBaseSalary":40000},{"applicantSecondName3":"Mohamed","applicantThirdName3":"Moubarak","applicantFirstName3":"Tariq","employeeId":"EMPLOYEE2","employeesFullName":"Tariq Mohamed Moubarak","confirm":false,"applicantEmployerContribution":10,"applicantEmployeeDeduction":24,"applicantHealth":2,"applicantInjury":4,"applicantPension":28.000000000000004,"applicantBaseSalary":200}]}}
	), 40200)
});

it('DatagridColumnTotal test, atleast one value NaN', () => {
	let params = ['data.applicantEditGrid', 'applicantTotalSumaIncertidumbresA1'];
	assert.strictEqual(jsonLogic.apply(
			{
				dataGridColumnTotal: [
					'data.applicantEditGrid_collection_applicantTotalSumaIncertidumbresA1'
				],
			},
			{"data": {"applicantEditGrid": [
						{"applicantTotalSumaIncertidumbresA1": 200 }, // taken into account
						{"applicantTotalSumaIncertidumbresA1": NaN },
						{"applicantTotalSumaIncertidumbresA1": {} },
						{"applicantTotalSumaIncertidumbresA1": () => {} },
						{"applicantTotalSumaIncertidumbresA1": [] },
						{"applicantTotalSumaIncertidumbresA1": "2" }, // taken into account
						{"applicantTotalSumaIncertidumbresA1": "3.5" }, // taken into account
						{"applicantTotalSumaIncertidumbresA1": "2e10" }, // taken into account
						{"applicantTotalSumaIncertidumbresA1": "hello" },
						{"applicantTotalSumaIncertidumbresA1": false },
						{"applicantTotalSumaIncertidumbresA1": true },
						{"applicantTotalSumaIncertidumbresA1": Infinity },
						{"applicantTotalSumaIncertidumbresA1": null },
						{"applicantTotalSumaIncertidumbresA1": undefined },
						{"applicantTotalSumaIncertidumbresA1": "" },
					]}}),
		20000000205.5);
});

it('gridRowCounter', () => {
	assert.strictEqual(jsonLogic.apply(
		{"gridRowCounter": "data.applicantDataGrid_collection_applicantBaseSalary"},
		{"data":{"applicantDataGrid": [{"applicantFirstName3":"Ali","employeeId":"EMPLOYEE1","employeesFullName":"Ali  ","confirm":false,"applicantEmployerContribution":2000,"applicantEmployeeDeduction":480,"applicantHealth":40,"applicantInjury":80,"applicantPension":560,"applicantBaseSalary":40000},{"applicantSecondName3":"Mohamed","applicantThirdName3":"Moubarak","applicantFirstName3":"Tariq","employeeId":"EMPLOYEE2","employeesFullName":"Tariq Mohamed Moubarak","confirm":false,"applicantEmployerContribution":10,"applicantEmployeeDeduction":24,"applicantHealth":2,"applicantInjury":4,"applicantPension":28.000000000000004,"applicantBaseSalary":200}]}}
	), 2)
});

it('gridRowCounter deal with empty object row', () => {
	// This method should test the situation where empty object in array is considered empty row and not
	// taken into account while calculating size of array
	assert.strictEqual(jsonLogic.apply(
		{
			"gridRowCounter": [
				"data.applicantMembers"
			]
		},
		{"data": {
				"applicantMembers": [{},{},{"hello": "world"}]
			}}), 1)
});

it('isEmptyObject 1', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"isEmptyObject": {"var": "data.applicantMembers"}
		},
		{"data": {"applicantMembers": {} }}), true)
});
it('isEmptyObject 2', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"isEmptyObject": {"var": "data.applicantMembers"}
		},
		{"data": {"applicantMembers": 1 }}), false)
});
it('isEmptyObject 3', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"isEmptyObject": {"var": "data.applicantMembers"}
		},
		{"data": {"applicantMembers": false }}), false)
});
it('isEmptyObject 4', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"isEmptyObject": {"var": "data.applicantMembers"}
		},
		{"data": {"applicantMembers": null }}), false)
});
it('isEmptyObject 5', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"isEmptyObject": {"var": "data.applicantMembers"}
		},
		{"data": {"applicantMembers": undefined }}), false)
});
it('isEmptyObject 5', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"isEmptyObject": {"var": "data.applicantMembers"}
		},
		{"data": {"applicantMembers": '' }}), false)
});

it('isEmptyObject 5', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"isEmptyObject": {"var": "data.applicantMembers"}
		},
		{"data": {"applicantMembers": [] }}), false)
});

it('penaltyMonths use ceil', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyMonths": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				},
				{
					"var": "multiplier"
				},
				{
					"var": "useCeil"
				},
			]
		},
		{"expiry": "2021-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z", "multiplier": 1000, "useCeil": true}), 1000)
});

it('penaltyWeeks use floor', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyWeeks": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				},
				{
					"var": "multiplier"
				},
				{
					"var": "useCeil"
				},
			]
		},
		{"expiry": "2021-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z", "multiplier": 1000, "useCeil": false}), 1000)
});

it('penaltyWeeks use ceil', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyWeeks": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				},
				{
					"var": "multiplier"
				},
				{
					"var": "useCeil"
				},
			]
		},
		{"expiry": "2021-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z", "multiplier": 1000, "useCeil": true}), 2000)
});

it('penaltyDays use ceil', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyDays": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				},
				{
					"var": "multiplier"
				},
				{
					"var": "useCeil"
				},
			]
		},
		{"expiry": "2021-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z", "multiplier": 1000, "useCeil": true}), 8000)
});

it('penaltyDays use floor', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyDays": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				},
				{
					"var": "multiplier"
				},
				{
					"var": "useCeil"
				},
			]
		},
		{"expiry": "2021-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z", "multiplier": 1000, "useCeil": false}), 7000)
});

it('penaltyMonths use ceil today is in the future', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyMonths": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				},
				{
					"var": "multiplier"
				},
				{
					"var": "useCeil"
				},
			]
		},
		{"expiry": "2022-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z", "multiplier": 1000, "useCeil": true}), 0)
});

it('penaltyMonths use floor', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyMonths": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				},
				{
					"var": "multiplier"
				},
				{
					"var": "useCeil"
				},
			]
		},
		{"expiry": "2021-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z", "multiplier": 1000, "useCeil": false}), 0)
});

it('penaltyMonths use floor today is in the future', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyMonths": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				},
				{
					"var": "multiplier"
				},
				{
					"var": "useCeil"
				},
			]
		},
		{"expiry": "2022-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z", "multiplier": 1000, "useCeil": false}), 0)
});

it('penaltyMonths multiplier missing', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"penaltyMonths": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				}
			]
		},
		{"expiry": "2021-12-31T00:00:00.000Z", "today": "2022-01-07T07:46:34.000Z"}), 0)
});

it('maxValue', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"maxValue": [
				{
					"var": "expiry"
				},
				{
					"var": "today"
				}
			]
		},
		{"expiry": "2022-01-07T07:46:34.000Z", "today": "2021-12-31T00:00:00.000Z"}), "2022-01-07T07:46:34.000Z")
});

it('defaultZero', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"defaultZero": [
				{
					"var": "expiry"
				}
			]
		},
		{"expiry": "2022-01-07T07:46:34.000Z"}), 0)
});

it('defaultZero empty string', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"round": [
				{
					"+": [
						0,
						{
							"+": [
								{
									"+": [
										{
											"defaultZero": [
												{
													"var": "applicantCash5"
												}
											]
										},
										{
											"defaultZero": [
												{
													"var": "applicantInKind17"
												}
											]
										}
									]
								},
								{
									"defaultZero": [
										{
											"var": "applicantNoContr2"
										}
									]
								}
							]
						}
					]
				},
				3
			]
		},
		{
			"applicantNoContr2": "",
			"applicantInKind17": undefined,
			"applicantCash5": 90
		}), 90)
});


it('defaultZero value missing', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"defaultZero": [
				{
					"var": "expiry"
				}
			]
		},
		{}), 0)
});

it('compareArrays not equal', () => {
	assert.strictEqual(jsonLogic.apply(
		{ "compareArrays": [ { "var": "arrayOne" }, { "var": "arrayTwo" } ] },
		{ "arrayOne": [1, 1, 2], "arrayTwo": [1, 2, 2] }), false)
});

it('compareArrays not equal again', () => {
	assert.strictEqual(jsonLogic.apply(
		{ "compareArrays": [ { "var": "arrayOne" }, { "var": "arrayTwo" } ] },
		{ "arrayOne": [1, 2], "arrayTwo": [1, 2, 3] }), false)
});

it('compareArrays not equal yet again', () => {
	assert.strictEqual(jsonLogic.apply(
		{ "compareArrays": [ { "var": "arrayOne" }, { "var": "arrayTwo" } ] },
		{ "arrayOne": [1, 2, 3], "arrayTwo": [1, 2] }), false)
});

it('compareArrays equal', () => {
	assert.strictEqual(jsonLogic.apply(
		{ "compareArrays": [ { "var": "arrayOne" }, { "var": "arrayTwo" } ] },
		{ "arrayOne": [1, 2, 3], "arrayTwo": [1, 2, 3] }), true)
});

it('compareArrays equal again', () => {
	assert.strictEqual(jsonLogic.apply(
		{ "compareArrays": [ { "var": "arrayOne" }, { "var": "arrayTwo" } ] },
		{ "arrayOne": [1, 2, 3], "arrayTwo": [3, 1, 2] }), true)
});

it('rounduphundred exact', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"rounduphundred": [
				{
					"var": "input"
				}
			]
		},
		{"input": 400}), 4)
});

it('rounduphundred not exact', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"rounduphundred": [
				{
					"var": "input"
				}
			]
		},
		{"input": 403}), 5)
});

it('roundupthousand exact', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"roundupthousand": [
				{
					"var": "input"
				}
			]
		},
		{"input": 40000}), 40)
});

it('roundupthousand not exact', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"roundupthousand": [
				{
					"var": "input"
				}
			]
		},
		{"input": 40003}), 41)
});

it('rounduphundredthousand exact', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"rounduphundredthousand": [
				{
					"var": "input"
				}
			]
		},
		{"input": 400000}), 4)
});

it('rounduphundredthousand not exact', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"rounduphundredthousand": [
				{
					"var": "input"
				}
			]
		},
		{"input": 40003}), 1)
});

it('createObject', () => {
    assert.deepEqual(jsonLogic.apply({
        "createObject": ["foo", "bar"]
    }), { foo: "bar" });
});

it('getKeyFromCatalog', () => {
	assert.deepEqual(jsonLogic.apply({"getKeyFromCatalog": [{
			"var": "row.applicantCatalogue"
		}]}, {"row": {
			"applicantname": "rauno",
			"applicantCatalogue": {
				"key": "2c9280858b048f35018b04e43f76092f",
				"value": "Åland Islands"
			}
		}}), "2c9280858b048f35018b04e43f76092f");
});

it('getValueFromCatalog', () => {
	assert.deepEqual(jsonLogic.apply({"getValueFromCatalog": [{
			"var": "row.applicantCatalogue"
		}]}, {"row": {
			"applicantname": "rauno",
			"applicantCatalogue": {
				"key": "2c9280858b048f35018b04e43f76092f",
				"value": "Åland Islands"
			}
		}}), "Åland Islands");
});

it("createObject in context", () => {
  assert.deepEqual(
    jsonLogic.apply(
      {
        merge: [
          { createObject: [{ var: "data.foo" }, { var: "data.bar" }] },
          { createObject: [{ var: "data.bar" }, { var: "data.foo" }] },
        ],
      },
      { data: { foo: "foo", bar: "bar" } }
    ),
    [{ foo: "bar" }, { bar: "foo" }]
  );
});

it("convertData in context", () => {
	assert.deepEqual(
		jsonLogic.apply(
			{
				convertData: [
					{ var: "data.list" },
					{foo1: "foo", bar1: "bar"}
				],
			},
			{ data: { list: [{ foo: "foo-1", bar: "bar-1" }, { foo: "foo-2", bar: "bar-2" }] } }
		),
		[
			{
				bar1: 'bar-1',
				foo1: 'foo-1'
			},
			{
				bar1: 'bar-2',
				foo1: 'foo-2'
			}
		]
	);
	assert.deepEqual(
		jsonLogic.apply(
			{
				convertData: [
					{ var: "data.list" },
					{foo1: ["foo", ""], bar1: "bar"}
				],
			},
			{ data: { list: [{}] } }
		),
		[
			{
				foo1: ''
			}
		]
	);
	assert.deepEqual(
		jsonLogic.apply(
			{
				convertData: [
					{ var: "data.list" },
					{"": null, onePerson: ['person', {}, {"Full name": ['name', '']}]}
				],
			},
			{ data: { list: [{person: {name: "John Smith"}}, {}] } }
		),
		[
			{
				onePerson: {
					'Full name': 'John Smith'
				}
			},
			{
				onePerson: {
					'Full name': ''
				}
			}
		]
	);
});

it("checks businessDaysDiff", async () => {
  await new Promise((resolve) => setTimeout(resolve, 500));
  assert.strictEqual(
    jsonLogic.apply(
      {
        businessDaysDiff: [
          {
            var: "from",
          },
          {
            var: "to",
          },
        ],
      },
      {
        from: "2022-10-10T00:00:00.000Z",
        to: "2022-10-20T00:00:00.000Z",
      }
    ),
    7
  );

  assert.strictEqual(
    jsonLogic.apply(
      {
        businessDaysDiff: [
          {
            var: "from",
          },
          {
            var: "to",
          },
        ],
      },
      {
        from: "2019-01-01T00:00:00.000Z",
        to: "2019-02-11T00:00:00.000Z",
      }
    ),
    29
  );
});

it("checks workingTimeDiff", async () => {
  await new Promise((resolve) => setTimeout(resolve, 500));
  assert.strictEqual(
    jsonLogic.apply(
      {
        workingTimeDiff: [
          {
            var: "from",
          },
          {
            var: "to",
          },
        ],
      },
      {
        from: "2022-10-10T10:00:00.000Z",
        to: "2022-10-10T11:00:00.000Z",
      }
    ),
    3600000
  );
  assert.strictEqual(
    jsonLogic.apply(
      {
        workingTimeDiff: [
          {
            var: "from",
          },
          {
            var: "to",
          },
        ],
      },
      {
        from: "2022-10-10T15:00:00.000Z",
        to: "2022-10-10T19:00:00.000Z",
      }
    ),
    10800000
  );
  assert.strictEqual(
    jsonLogic.apply(
      {
        workingTimeDiff: [
          {
            var: "from",
          },
          {
            var: "to",
          },
        ],
      },
      {
        from: "2022-10-10T15:00:00.000Z",
        to: "2022-10-12T12:00:00.000Z",
      }
    ),
    44940000
  );
  //Outside working hours
  assert.strictEqual(
    jsonLogic.apply(
      {
        workingTimeDiff: [
          {
            var: "from",
          },
          {
            var: "to",
          },
        ],
      },
      {
        from: "2022-10-13T07:00:00.000Z",
        to: "2022-10-13T10:30:00.000Z",
      }
    ),
    0
  );
  assert.strictEqual(
    jsonLogic.apply(
      {
        workingTimeDiff: [
          {
            var: "from",
          },
          {
            var: "to",
          },
        ],
      },
      {
        from: "2022-10-10T07:00:00.000Z",
        to: "2022-10-10T20:00:00.000Z",
      }
    ),
    28800000
  );
});

it("checks workingTimeDiff", async () => {
	await new Promise((resolve) => setTimeout(resolve, 500));
	assert.strictEqual(
		jsonLogic.apply(
			{
				workingHoursDiff: [
					{
						var: "from",
					},
					{
						var: "to",
					},
				],
			},
			{
				from: "2022-10-10T10:00:00.000Z",
				to: "2022-10-10T11:00:00.000Z",
			}
		),
		1
	);
	assert.strictEqual(
		jsonLogic.apply(
			{
				workingHoursDiff: [
					{
						var: "from",
					},
					{
						var: "to",
					},
				],
			},
			{
				from: "2022-10-10T10:00:00.000Z",
				to: "2022-10-10T10:30:00.000Z",
			}
		),
		0.5
	);
	assert.strictEqual(
		jsonLogic.apply(
			{
				workingHoursDiff: [
					{
						var: "from",
					},
					{
						var: "to",
					},
				],
			},
			{
				from: "2022-10-10T15:00:00.000Z",
				to: "2022-10-10T19:00:00.000Z",
			}
		),
		3
	);
	assert.strictEqual(
		jsonLogic.apply(
			{
				workingHoursDiff: [
					{
						var: "from",
					},
					{
						var: "to",
					},
				],
			},
			{
				from: "2022-10-10T15:00:00.000Z",
				to: "2022-10-12T12:31:00.000Z",
			}
		),
		13
	);
	//Outside working hours
	assert.strictEqual(
		jsonLogic.apply(
			{
				workingHoursDiff: [
					{
						var: "from",
					},
					{
						var: "to",
					},
				],
			},
			{
				from: "2022-10-13T07:00:00.000Z",
				to: "2022-10-13T10:30:00.000Z",
			}
		),
		0
	);
	assert.strictEqual(
		jsonLogic.apply(
			{
				workingHoursDiff: [
					{
						var: "from",
					},
					{
						var: "to",
					},
				],
			},
			{
				from: "2022-10-10T07:00:00.000Z",
				to: "2022-10-10T20:00:00.000Z",
			}
		),
		8
	);
});

it('AddYears test', () => {
	assert.strictEqual(jsonLogic.apply(
			{
				"if": [
					{
						"==": [
							{
								"cat": [
									"",
									{
										"var": "data.applicantRadioDuration"
									}
								]
							},
							"fixed"
						]
					},
					{
						"addYears": [
							{
								"var": "data.applicantToday"
							},
							{
								"var": "data.applicantDuration"
							}
						]
					},
					0
				]
			},
			{"data":{"userid":"13598","username":"rdina","datetimenow":"2023-03-29t15:44:12.479813","dossierdate":"2023-03-29","starteremail":"dina.ps.roxana@gmail.com","userfullname":"roxana dina","userlastname":"dina","dossiernumber":"er28931","userfirstname":"roxana","dateafter1year":"2024-03-29","applicant-email":"dina.ps.roxana@gmail.com","applicantuserid":"13598","dateAfter2Years":"2025-03-29","applicantfullname":"roxana dina","applicantusername":"rdina","CURRENT_LANGUAGE":"en","is_submit_allowed":true,"totalCost":516215,"numberOfConditions":0,"mandatorynameClearance":true,"mandatorynameClearancehidden":"Name clearance","mandatoryregisterALlcAtTheCompanyRegistry":true,"mandatoryregisterALlcAtTheCompanyRegistryhidden":"Register a LLC at the Company Registry","license-url_2c928088872c24e701872c94dd790178-establishmentDecision":"print2c928088872c24e701872c94dd7901783p16p37979928","license-url_2c928088872c24e701872c94ddef0179-llcRegistrationCertificate":"print2c928088872c24e701872c94ddef01793p16p37979928","applicantToday":"2023-03-29T13:44:13.000Z","applicantExpiration":"2200-01-01","Is form valid?":false,"isCurrentPartATabValid":true,"isSubmittable":true,"price_nameClearancenameClearanceFee":6000,"price_registerALlcAtTheCompanyRegistrycompanyArticlesStudyMinistryOfFinance":10000,"price_registerALlcAtTheCompanyRegistrycompanyArticlesStudyMinistryOfTrade":500000,"price_registerALlcAtTheCompanyRegistrystampFee":215,"nameClearance":true,"registerALlcAtTheCompanyRegistry":true,"numberOfRegistrations":2,"guideMenuRegistrations":"mandatorynameClearance;mandatoryregisterALlcAtTheCompanyRegistry","numberOfRequirements":8,"numberOfUploadableRequirements":8,"guideMenuRequirements":"Payment receipt for name clearance;Articles of association;ID for each shareholder and director;Clearance from the ministry of Justice for each Director;Location clearance (municipality);Payment receipt for registration fees;Payment receipt for publication (official Gazette);Capital payment receipt;","guideMenuCosts":"Name clearance fee:price_nameClearancenameClearanceFee;Company articles study - ministry of finance:price_registerALlcAtTheCompanyRegistrycompanyArticlesStudyMinistryOfFinance;Company articles study - ministry of trade:price_registerALlcAtTheCompanyRegistrycompanyArticlesStudyMinistryOfTrade;Stamp fee:price_registerALlcAtTheCompanyRegistrystampFee;","listing-value":"","applicantValuePerShare2":null,"applicantNumberOfDirectors2":0,"navProgress":[100,100],"progressCalc0":100,"isFormValid":false,"filetitlearticlesOfAssociation":"Articles of association","filetitleidForEachShareholderAndDirector":"ID for each shareholder and director","filetitleclearanceFromTheMinistryOfJusticeForEachDirector":"Clearance from the ministry of Justice for each Director","filetitleworkPermit":"Work permit","filetitlelocationClearanceMunicipality":"Location clearance (municipality)","filetitlepaymentReceiptForRegistrationFees":"Payment receipt for registration fees","filetitlepaymentReceiptForPublicationOfficialGazette":"Payment receipt for publication (official Gazette)","filetitlecertificateFromTheShopsRegister":"Certificate from the shops register","filetitlearticlesOfAssociationCorporateShareholder":"Articles of association (corporate shareholder)","filetitleboardDecisionToBeAShareholderInTheNewCompanyCorporateShareholder":"Board decision to be a shareholder in the new company (corporate shareholder)","saveDocuments":false,"suffixCurrency":"$","feesDescription":{},"requiredDocumentsContainer":{},"guideWhatTypeOfCompanyWouldYouLikeToOpen":{"key":"01","value":"Limited Liability Company (LLC)"},"applicantCompanyType":{"key":"01","value":"Limited Liability Company (LLC)"},"guideOnePersonLlc":"no","guideOffshoreCompany":"no","guideCompanyCapital":50000000,"applicantCapital":50000000,"guideWillYouHaveAShop":"no","guideWillACompanyBeOneOfTheShareholders":"no","btnValidateGuide":true,"progressCalc1":100,"applicantRadioDuration":"fixed","applicantDuration":67}}),
		'2090-03-29T13:44:13.000Z')
});

it('DatagridColumnTotal test, one line without value', () => {
	assert.strictEqual(jsonLogic.apply(
			{
				"dataGridColumnTotal": [
					"data.applicantDataGrid_collection_applicantAge"
				]
			},
		{"data": {
			"userid": "5a902bd0-8b9c-4b75-a17d-4c1bca8b0f8f",
			"username": "supermario",
			"applicantDataGrid": [
				{
					"applicantName": "",
					"applicantAge": 3
				},
				{
					"applicantAge": 4
				},
				{
					"applicantAge": 5
				},
				{
					"applicantAge": undefined
				}
			]
		}}),
		12);
});

it('datagridcolumntotal deal with nulls', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"round": [
				{
					"+": [
						0,
						{
							"dataGridColumnTotal": [
								"data.applicantEditGrid_collection_applicantTotalSumaIncertidumbresA1"
							]
						}
					]
				},
				3
			]
		},
		{"data": {
				"applicantEditGrid": [{},
					{"applicantTotalSumaIncertidumbresA1": undefined},
					{"applicantTotalSumaIncertidumbresA1": ""},
					{"applicantTotalSumaIncertidumbresA1": 23232},
					{"applicantTotalSumaIncertidumbresA1": null}]
			}}), 23232)
});

it('extractValueFromGrid', () => {
	assert.strictEqual(jsonLogic.apply(
		{
			"extractValueFromGrid": [{"var": "data.applicantEditGrid"}, "sumOfNumber", "typeOfShares", "class d"]
		},
		{"data": {
				"applicantEditGrid": [
					{"typeOfShares": "Class A", "sumOfNumber": 101},
					{"typeOfShares": "class B", "sumOfNumber": 102},
					{"typeOfShares": "Class C", "sumOfNumber": 103},
					{"typeOfShares": "class d", "sumOfNumber": 104}]
			}}), 104)
});



