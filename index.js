"use strict";

var jsonLogic = require("json-logic-js-base");
var moment = require("moment");
var axios = require("axios");
var currentWorkingTime = null;
var currentWorkingTimeResolved = null;
var WORKING_TIME_POLL_INTERVAL = 60000;
var WORKING_TIME_URL = typeof process === "undefined" ? "" : process.env.WORKING_TIME_URL;
var IS_TESTING = typeof process === "undefined" ? false : process.env.IS_TESTING;
var RESTHEART_LOCATION = "http://restheart:8080";

var setupWorkingTime = function () {
  if (currentWorkingTime) return;
  getWorkingTime().finally(function () {
    if (currentWorkingTime) return;
    setTimeout(setupWorkingTime, WORKING_TIME_POLL_INTERVAL);
  });
};

var getWorkingTime = () => {
  var result;
  let location = null;
  try {
    location = window && window.location;
  } catch (err) {
    if (!IS_TESTING && !WORKING_TIME_URL) {
      return;
    }
    if (err instanceof ReferenceError) {
      console.log("Executed outside browser");
    }
  }
  if (location) {
    if (location.origin === "http://localhost:8080") { // when used inside headless chrome from DS container
      location = RESTHEART_LOCATION;
    } else {
      location = location.origin;
    }
    result = axios.get(
      `${location}/restheart/documents/settings/workingtime`
    );
  } else {
    if (IS_TESTING) {
      currentWorkingTime = require(WORKING_TIME_URL);
      result = Promise.resolve({ data: currentWorkingTime });
    } else {
      result = axios.get(`${WORKING_TIME_URL}`);
    }
  }
  return result
    .then(function (response) {
      currentWorkingTime = response.data;
      if (!currentWorkingTimeResolved) {
        currentWorkingTimeResolved = JSON.parse(
          JSON.stringify(currentWorkingTime)
        );
      }
      currentWorkingTimeResolved.holidays = [];
      for (const holiday of currentWorkingTime.holidays) {
        currentWorkingTimeResolved.holidays.push({
          ...holiday,
          fromDate: moment.utc(holiday.fromDate).startOf("day"),
          toDate: moment.utc(holiday.toDate).endOf("day"),
        });
      }
      if (!currentWorkingTimeResolved.workingDaysInWeek) {
        currentWorkingTimeResolved.workingDaysInWeek = [];
      }
      // Map to JS day of week format with Sunday - 0, Monday - 1...
      currentWorkingTimeResolved.workingDaysInWeek =
        currentWorkingTimeResolved.workingDaysInWeek.map(
          (oneBasedIndexedDay) => {
            if (Number(oneBasedIndexedDay) === 7) return 0;
            return Number(oneBasedIndexedDay);
          }
        );
      var workingDays = {};
      currentWorkingTimeResolved.workingDays.forEach((workingDay) => {
        if (workingDay.weekDay === 7) {
          workingDays[0] = workingDay;
        } else {
          workingDays[workingDay.weekDay] = workingDay;
        }
      });
      currentWorkingTimeResolved.workingDays = workingDays;
    })
    .catch(function (error) {
      console.log(error);
    });
};

var isWorkingDay = (date) => {
  var workingDaysInWeek = currentWorkingTimeResolved.workingDaysInWeek;

  if (!workingDaysInWeek.includes(date.day())) {
    return false;
  }
  if (
    currentWorkingTimeResolved.holidays.some(
      (holiday) => date >= holiday.fromDate && date <= holiday.toDate
    )
  ) {
    return false;
  }

  return true;
};

getWorkingTime();

var setupWorkingDay = function (date, isStart) {
  var workingDay = moment.utc(date);
  var workingDayTime =
    currentWorkingTimeResolved.workingDays[workingDay.day()][
      isStart ? "startAm" : "endPm"
    ].split(":");
  workingDay.set({
    h: workingDayTime[0],
    m: workingDayTime[1],
  });
  return workingDay;
};

module.exports = jsonLogic;

function convertData(data, dataMapping) {
  if (data == null || dataMapping == null) {
    return undefined;
  }
  if (Array.isArray(data)) {
    return data
      .map((item) => convertData(item, dataMapping))
      .filter((item) => item != null);
  } else if (typeof data === "object") {
    if (Array.isArray(dataMapping)) {
      const key = dataMapping[0];
      if (key == null) {
        return undefined;
      }
      let value = data[key];
      if (value === undefined) {
        value = dataMapping[1];
      }
      if (value !== undefined && dataMapping[2]) {
        value = convertData(value, dataMapping[2]);
      }
      return value;
    } else if (typeof dataMapping === "object") {
      const newData = {};
      for (const key of Object.keys(dataMapping)) {
        const value = convertData(data, dataMapping[key]);
        if (value === undefined) {
          continue;
        }
        newData[key] = value;
      }
      return newData;
    } else if (typeof dataMapping === "string") {
      return data[dataMapping];
    }
  }
}

jsonLogic.customOperations = {
  convertData,
  Date: Date,
  date_parse: function (someDateParam) {
    if (someDateParam == null || someDateParam === "") return null;
    return Date.parse(someDateParam);
  },
  // Retrieve Any Date
  getDate: function (date) {
    if (date == null) return date
    if (date === "") return null;
    // we need to cut the timezone here.
    if (typeof date === "string") {
      if (date.match(/^(\d\d\d\d-\d\d-\d\dT\d\d:\d\d:\d\d).*/)) {
        date = date.substring(0, 19) + ".000Z";
      }
    }
    return moment.utc(date).toISOString();
  },
  daysDiff: function (future, past, fn) {
    if (future == null || future === "" || past == null || past === "")
      return 0;
    fn = fn ? Math.ceil : Math.floor;
    return Math.max(
      fn(moment.utc(future).diff(moment.utc(past), "days", true)),
      0
    );
  },
  weeksDiff: function (future, past, fn) {
    if (future == null || future === "" || past == null || past === "")
      return 0;
    fn = fn ? Math.ceil : Math.floor;
    return Math.max(
      fn(moment.utc(future).diff(moment.utc(past), "weeks", true)),
      0
    );
  },
  monthsDiff: function (future, past, fn) {
    if (future == null || future === "" || past == null || past === "")
      return 0;
    fn = fn ? Math.ceil : Math.floor;
    return Math.max(
      fn(moment.utc(future).diff(moment.utc(past), "months", true)),
      0
    );
  },
  yearsDiff: function (future, past, fn) {
    if (future == null || future === "" || past == null || past === "")
      return 0;
    fn = fn ? Math.ceil : Math.floor;
    return Math.max(
      fn(moment.utc(future).diff(moment.utc(past), "years", true)),
      0
    );
  },
  yearFromDate: function (date) {
    if (date == null || date === "") return 0;
    return moment.utc(date).year();
  },
  monthFromDate: function (date) {
    if (date == null || date === "") return 0;
    return moment.utc(date).month() + 1;
  },
  dayFromDate: function (date) {
    if (date == null || date === "") return 0;
    return moment.utc(date).date();
  },
  substractDays: function (date, nrOfDays) {
    date = jsonLogic.customOperations.getDate(date);
    if (!date) return date;
    return moment
      .utc(date)
      .subtract(nrOfDays || 0, "days")
      .toISOString();
  },
  addDays: function (date, nrOfDays) {
    date = jsonLogic.customOperations.getDate(date);
    if (!date) return date;
    return moment
      .utc(date)
      .add(nrOfDays || 0, "days")
      .toISOString();
  },
  substractMonths: function (date, nrOfMonths) {
    date = jsonLogic.customOperations.getDate(date);
    if (!date) return date;
    return moment
      .utc(date)
      .subtract(nrOfMonths || 0, "months")
      .toISOString();
  },
  addMonths: function (date, nrOfMonths) {
    date = jsonLogic.customOperations.getDate(date);
    if (!date) return date;
    return moment
      .utc(date)
      .add(nrOfMonths || 0, "months")
      .toISOString();
  },
  substractYears: function (date, nrOfYears) {
    date = jsonLogic.customOperations.getDate(date);
    if (!date) return date;
    return moment
      .utc(date)
      .subtract(nrOfYears || 0, "years")
      .toISOString();
  },
  addYears: function (date, nrOfYears) {
    date = jsonLogic.customOperations.getDate(date);
    if (!date) return date;
    return moment
      .utc(date)
      .add(nrOfYears || 0, "years")
      .toISOString();
  },
  overrideYear: function (date, year) {
    date = jsonLogic.customOperations.getDate(date);
    if (!date) return date;
    var d = moment.utc(date);
    if (year != null) d.year(year);
    return d.toISOString();
  },
  overrideDate: function(date, year, month, day, hour, minute) {
    date = jsonLogic.customOperations.getDate(date);
    if (!date) return date;

    var d = moment.utc(date);

    if (year != null && year !== 0) d.year(year);
    if (month != null && month !== 0) d.month(month - 1); // moment.js uses zero-based months (0-11)
    if (day != null && day !== 0) d.date(day);

    if (hour != null) d.hour(hour);
    if (minute != null) d.minute(minute);

    return d.toISOString();
  },
  formatDateDDdashMMdashYYYY: function (date) {
    if (date == null || date === "") return null;
    return moment.utc(date).format("DD-MM-YYYY");
  },
  formatDate: function (date, formatFrom, formatTo) {
    if (date == null || date === "") return null;
    return moment.utc(date, formatFrom).format(formatTo);
  },
  firstApplicableDate: function () {
    var args = Array.prototype.slice.call(arguments);
    var matcher = /^\d{4}-[01]\d-[0-3]\d/;
    return (
      args.find(function (arg) {
        if (typeof arg === "string" || arg instanceof String) {
          var match = arg.match(matcher);
          return (match && match.length > 0) || false;
        }
        return arg instanceof Date;
      }) || null
    );
  },
  round: function (param, decimals) {
    if (param == null || param === "") return;
    return Number(param.toFixed(decimals));
  },
  ceil: function (param) {
    return Math.ceil(param);
  },
  floor: function (param) {
    return Math.floor(param);
  },
  parseInt: function (param) {
    return parseInt(param);
  },
  parseFloat: function (param) {
    return parseFloat(param);
  },
  join: function (array, separator) {
    if (array == null || !(array instanceof Array)) return "";
    return array.join(separator);
  },
  split: function (param, separator) {
    if (param == null || param === "") return [];
    return param.toString().split(separator);
  },
  replace: function (param, oldValue, newValue) {
    if (param == null) return "";
    var re = new RegExp(oldValue, "g");
    return param.toString().replace(re, newValue);
  },
  toUpperCase: function (param) {
    if (param == null) return "";
    return param.toString().toUpperCase();
  },
  toLowerCase: function (param) {
    if (param == null) return "";
    return param.toString().toLowerCase();
  },
  isNumber: function (value) {
    if (typeof value === 'boolean' || Array.isArray(value)) {
      return false;
    }
    // Try to parse the value into a number
    const parsedValue = Number(value);
    // Check if the parsed value is a finite number
    return Number.isFinite(parsedValue);
  },
  dataGridColumnTotal: function (param) {
    if (param == null || param === "" || typeof param !== "string") return 0;
    var params = param.split("_collection_");
    return jsonLogic.apply(
      {
        reduce: [
          { var: params[0] },
          {
            "+": [
              { "if": [
                  { "or": [
                      { "==": [{ var: `current.${params[1]}` }, null] },
                      { "===": [{ var: `current.${params[1]}` }, ""] },
                      { "===": [{ isNumber: { var: `current.${params[1]}` }}, false] }
                    ] },
                  0,
                  { var: `current.${params[1]}` }
                ]
              },
              { var: "accumulator" },
            ],
          },
          0,
        ],
      },
      this
    );
  },
  isEmptyObject: function (obj) {
    return obj != null && Object.keys(obj).length === 0 && obj.constructor === Object;
  },
  gridRowCounter: function (param) {
    if (param == null || param === "" || typeof param !== "string") return 0;
    var params = param.split('_collection_');
    return jsonLogic.apply({
      "reduce": [
        {"var": params[0]},
        {
          "+": [
            {"if": [
                {"isEmptyObject": {"var": "current"}},
                0,
                1
              ]},
            {"var": "accumulator"}
          ]
        },
        0
      ]
    }, this);
  },
  extractValueFromGrid: function (grid, columnName, filterByColumnName, filterByValue) {
    if (grid == null || grid === "" || !Array.isArray(grid)) return 0;
    const matchingRow = grid.find(item => item[filterByColumnName] === filterByValue);
    return matchingRow && matchingRow[columnName] || 0;
  },
  translate: function (inputArg) {
    if (typeof inputArg === "string") {
      // Input is a string
      return translate(inputArg);
    } else if (Array.isArray(inputArg)) {
      // Input is an array
      return inputArg.map(item => {
        if (typeof item === "string") {
          return translate(item);
        } else if (typeof item === "object" && item !== null && "value" in item) {
          return translate(item.value);
        } else {
          return "";
        }
      });
    } else if (typeof inputArg === "object" && inputArg !== null && "value" in inputArg) {
      // Input is an object
      return translate(inputArg.value);
    }
    return "";
  },
  penaltyMonths: function (past, future, multiplier, fn) {
    if (multiplier == null || multiplier === "") return 0;
    return jsonLogic.customOperations.monthsDiff(future, past, fn) * multiplier;
  },
  penaltyWeeks: function (past, future, multiplier, fn) {
    if (multiplier == null || multiplier === "") return 0;
    return jsonLogic.customOperations.weeksDiff(future, past, fn) * multiplier;
  },
  penaltyDays: function (past, future, multiplier, fn) {
    if (multiplier == null || multiplier === "") return 0;
    return jsonLogic.customOperations.daysDiff(future, past, fn) * multiplier;
  },
  maxValue: function (a, b) {
    return a > b ? a : b;
  },
  defaultZero: function (a) {
    return a == null || a === '' || !jsonLogic.customOperations.isNumber(a) ? 0 : a;
  },
  roundup: function (input, factor) {
    var modulus = input % factor;
    if (modulus === 0) return input / factor;
    return (input - modulus) / factor + 1;
  },
  rounduphundred: function (input) {
    return jsonLogic.customOperations.roundup(input, 100);
  },
  roundupthousand: function (input) {
    return jsonLogic.customOperations.roundup(input, 1000);
  },
  rounduphundredthousand: function (input) {
    return jsonLogic.customOperations.roundup(input, 100000);
  },
  _round: function (param) {
    return Math.round(param);
  },
  _ceil: function (param) {
    return Math.ceil(param);
  },
  _floor: function (param) {
    return Math.floor(param);
  },
  createObject: function (key, value) {
    var result = {};
    result[key] = value;
    return result;
  },
  getKeyFromCatalog: function (catalog) {
    return catalog && catalog.key || '';
  },
  getValueFromCatalog: function (catalog) {
    return catalog && catalog.value || '';
  },
  compareArrays: function (arrayOne, arrayTwo) {
    if (arrayOne.length !== arrayTwo.length) {
      return false;
    }
    let sortedArrOne = [...arrayOne].sort();
    let sortedArrTwo = [...arrayTwo].sort();
    for (let i = 0; i < sortedArrOne.length; i++) {
      if (sortedArrOne[i] !== sortedArrTwo[i]) {
        return false;
      }
    }
    return true;
  },
  /**
   * from date inclusive
   * to date exclusive
   * */
  businessDaysDiff: function (from, to) {
    if (!currentWorkingTimeResolved) {
      getWorkingTime();
      return -1;
    }
    var numWorkDays = 0;
    var currentDate = moment.utc(from).subtract(1, "days");
    to = moment.utc(to).subtract(1, "days");
    while (currentDate < to) {
      currentDate = currentDate.add(1, "days");
      if (isWorkingDay(currentDate)) {
        numWorkDays++;
      }
    }
    return numWorkDays;
  },
  workingHoursDiff: function (from, to) {
    return jsonLogic.customOperations.workingTimeDiff(from, to) / 1000 / 3600;
  },
  workingTimeDiff: function (from, to) {
    if (!currentWorkingTimeResolved) {
      getWorkingTime();
      return -1;
    }
    var workingTimeMs = 0;
    var startDate = moment.utc(from);
    var currentDate = moment.utc(from);
    to = moment.utc(to);

    while (currentDate.isSameOrBefore(to, "day")) {
      if (!isWorkingDay(currentDate)) {
        currentDate = currentDate.add(1, "days");
        continue;
      }
      var workingDayStart = setupWorkingDay(currentDate, true);
      var workingDayEnd = setupWorkingDay(currentDate, false);
      // full working day
      if (
        !currentDate.isSame(startDate, "day") &&
        !currentDate.isSame(to, "day")
      ) {
        workingTimeMs += workingDayEnd - workingDayStart;
        currentDate = currentDate.add(1, "days");
        continue;
      }
      var startTs = currentDate.valueOf();
      if (!currentDate.isSame(startDate, "day")) {
        startTs = workingDayStart.valueOf();
      }
      var endTs = to.valueOf();
      /**
       * Let's assume '-----' represents working day range, and '+' represents duration given by "from" "to" params.
       * Then we have following cases.
       *     -----
       * +++              // no overlap
       *           +++    // no overlap
       *   +++            // partial overlap
       *        +++       // partial overlap
       *      +++         // from - to fully contained in working day range
       *   ++++++++++     // working day fully contained in from - to range
       */
      if (startTs > workingDayEnd.valueOf() ||
        endTs < workingDayStart.valueOf()) {
        // pass
      } else if (
        startTs <= workingDayStart.valueOf() &&
        endTs <= workingDayEnd.valueOf()
      ) {
        workingTimeMs += endTs - workingDayStart.valueOf();
      } else if (
        startTs >= workingDayStart.valueOf() &&
        endTs >= workingDayEnd.valueOf()
      ) {
        workingTimeMs += workingDayEnd.valueOf() - startTs;
      } else if (
        startTs >= workingDayStart.valueOf() &&
        endTs <= workingDayEnd.valueOf()
      ) {
        workingTimeMs += endTs - startTs;
      } else {
        workingTimeMs += workingDayEnd - workingDayStart;
      }
      currentDate = currentDate.add(1, "days");
    }
    return workingTimeMs;
  },
  sqrt: function (param) {
    if (param == null || param === '') {
      return;
    }
    return Math.sqrt(param);
  },
  joinNullIfEmpty: function (values, separator=",") {
    if (!Array.isArray(values) || values.length === 0) {
      return null;
    }
    const nonNullValues = values.filter(v => v != null);
    if (nonNullValues.length === 0) {
      return null;
    }
    return nonNullValues.join(separator);
  },
  isComponentValid: function (compKey) {
    if (this == null || typeof this != 'object' || !this.instance || !this.instance.root) {
      return null;
    }
    const comp = this.instance.root.getComponent(compKey);
    if (!comp) {
      return null;
    }
    if (Array.isArray(comp.errors) && comp.errors.length > 0) {
      return false;
    }
    return comp.checkValidity(null, false, null, true);
  }
};

Object.keys(jsonLogic.customOperations).forEach(function (k) {
  jsonLogic.add_operation(k, jsonLogic.customOperations[k]);
});
